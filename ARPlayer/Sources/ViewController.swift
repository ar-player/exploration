//
//  ViewController.swift
//  ARPlayer
//
//  Created by Julia Vashchenko on 4/27/18.
//  Copyright © 2018 aronskaya. All rights reserved.
//

import UIKit
import ARKit

class ViewController: UIViewController {

    // rootNode is in World Origin
    // child nodes are positioned relatively to the rootNode
    let kDefaultPosition = SCNVector3.init(0, 0, -0.2)
    private var mode: Mode = .capsule

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var sceneView: ARSCNView!

    @IBAction func didPressChangeMode(_ sender: Any) {
        mode = mode.next()
        titleLabel.text = mode.rawValue
    }
    
    @IBAction func didTap(_ recognizer: UIGestureRecognizer) {
        let tapLocation = recognizer.location(in: sceneView)
        if let node = node(location: tapLocation) {
            node.removeFromParentNode()
            return
        }
        switch mode {
        case .boxes:
            if let coordinates = worldPoint(fromTap: tapLocation) {
                addBox(x: coordinates.x, y: coordinates.y, z: coordinates.z)
                return
            }

        case .deathStar:
            addDeathStar()
        case .capsule:
            addCapsule()
        }
    }

    private func node(location: CGPoint) -> SCNNode? {
        let hitTestResults = sceneView.hitTest(location)

        if let node = hitTestResults.first?.node {
            return node
        }

        return nil
    }

    private func worldPoint(fromTap location: CGPoint) -> (x: Float, y: Float, z: Float)? {
        let hitTestResults = sceneView.hitTest(location, types: [.featurePoint, .existingPlane])

        if let result = hitTestResults.first {
            let translation = result.worldTransform.translation
            return (x: translation.x, y: translation.y, z:translation.z)
        }

        return nil
    }

    func addDeathStar() {
        let node = DeathStarNode()
        node.position = kDefaultPosition
        sceneView.scene.rootNode.addChildNode(node)
    }

    func addBox(x: Float = 0, y: Float = 0, z: Float = -0.2) {
        let node = SCNNode()
        node.geometry = SCNBox(width: 0.2, height: 0.2, length: 0.2, chamferRadius: 0) // in meters
        // light that is reflected off of a surface
        node.geometry?.firstMaterial?.specular.contents = UIColor.green
        //diffuse represents surface color
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.lagoon
        node.position = SCNVector3.init(x, y, z)
        sceneView.scene.rootNode.addChildNode(node)
    }

    func addCapsule() {
        let node = SCNNode()
        node.geometry = SCNCapsule(capRadius: 0.2, height: 0.8)
        node.geometry?.firstMaterial?.specular.contents = UIColor.lightYellow
        node.geometry?.firstMaterial?.diffuse.contents = UIColor.lightRed
        node.position = kDefaultPosition
        sceneView.scene.rootNode.addChildNode(node)
    }

    // MARK: Override

    override func viewDidLoad() {
        super.viewDidLoad()
        sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints, ARSCNDebugOptions.showWorldOrigin]
        sceneView.autoenablesDefaultLighting = true
        sceneView.showsStatistics = true
        let configuration = ARWorldTrackingConfiguration()
        sceneView.session.run(configuration)
        titleLabel.text = mode.rawValue
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        sceneView.session.pause()
    }
}

private enum Mode: String {
    case deathStar = "Death Star mode"
    case capsule = "Capsule mode"
    case boxes = "Boxes mode"

    func next() -> Mode {
        switch self {
        case .deathStar:
            return .capsule
        case .capsule:
            return .boxes
        case .boxes:
            return .deathStar
        }
    }
}

