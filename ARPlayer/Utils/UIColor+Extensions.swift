//
//  UIColor+Extensions.swift
//  ARPlayer
//
//  Created by Julia Vashchenko on 28/05/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

import UIKit

extension UIColor {
    static let lagoon = UIColor(red: 99/255, green: 202/255, blue: 208/255, alpha: 1.0)

    static let lightRed = UIColor(red: 234/255, green: 75/255, blue: 35/255, alpha: 1.0)

    static let lightYellow = UIColor(red: 244/255, green: 204/255, blue: 36/255, alpha: 1.0)
}

