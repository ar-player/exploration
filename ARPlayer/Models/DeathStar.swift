//
//  DeathStar.swift
//  ARPlayer
//
//  Created by Julia Vashchenko on 20/05/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

import SceneKit

final class DeathStarNode: SCNReferenceNode {
    init() {
        guard let url = Bundle.main.url(forResource: "DeathStar", withExtension: "scn", subdirectory: "Models.scnassets")
            else {
                fatalError("Missing expected bundle resource")

        }
        super.init(url: url)!
        load()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("\(#function) has not been implemented")
    }
}
