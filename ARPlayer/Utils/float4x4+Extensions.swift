//
//  float4x4+Extensions.swift
//  ARPlayer
//
//  Created by Julia Vashchenko on 28/05/2018.
//  Copyright © 2018 Julia Vashchenko. All rights reserved.
//

import simd

extension float4x4 {
    var translation: float3 {
        let translation = self.columns.3
        return float3(translation.x, translation.y, translation.z)
    }
}
